﻿
using DoAnCoSoPhanModel_DB.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace DoAnCoSoPhanModel_DB.DataAccess
{
    public class ApplicationDbContext : DbContext { 
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<CategorySubject> categorySubjects { get; set; }
        public DbSet<Course> courses { get; set; }
        public DbSet<Documents> documents { get; set; }
        public DbSet<Feedbacks> feedbacks { get; set; }
        public DbSet<News> news { get; set; }
        public DbSet<Post> posts { get; set; }
        public DbSet<Student> students { get; set; }
        public DbSet<Subject> subjects { get; set; }
        public DbSet<Teacher> teachers { get; set; }
        public DbSet<TeacherImage> teacherImages { get; set; }
        public DbSet<StudentImage> studentImages { get; set; }

    }
}
