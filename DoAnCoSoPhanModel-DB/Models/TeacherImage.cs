﻿using DoAnCoSoPhanModel_DB.Models;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class TeacherImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int teacherId { get; set; }
        public Teacher? teacher { get; set; }
    }
}
