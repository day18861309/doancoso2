﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class News
    {
        public Guid Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string Content { get; set; }

        [ForeignKey("AuthorId")]
        public int UserId { get; set; }

        public DateTime PublishDate { get; set; }

    }
}
