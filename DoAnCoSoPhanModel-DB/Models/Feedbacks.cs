﻿using System.ComponentModel.DataAnnotations.Schema;

using System.ComponentModel.DataAnnotations;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Feedbacks
    {
        public int Id { get; set; }

        [ForeignKey("UserId")]
        public string UserId { get; set; }

        public string FeedbackCourse { get; set; }

        public DateTime FeedbackDate { get; set; }
    }
}
