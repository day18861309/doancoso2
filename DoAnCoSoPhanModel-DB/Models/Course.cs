﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Course
    {
        public Guid Id { get; set; }

        [Required]
        public string CourseName { get; set; }

        [ForeignKey("SubjectId")]
        public int SubjectId { get; set; }

        public string Schedule { get; set; }
    }
}
