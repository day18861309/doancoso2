﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Documents
    {
        public Guid Id { get; set; }

        [Required]
        public string Content { get; set; }

        [ForeignKey("subjectId")]
        public int SubjectId { get; set; }

        [ForeignKey("TeacherId")]
        public int TeacherId { get; set; }

        [ForeignKey("CourseId")]
        public int CourseId { get; set; }

        public string FileType { get; set; }

        public string Description { get; set; }

        public string File {  get; set; }
    }
}
