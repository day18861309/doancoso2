﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Numerics;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
        public DbSet<CategorySubject> categorySubjects { get; set; }
        public DbSet<Course> courses { get; set; }
        public DbSet<Documents> documents { get; set; }
        public DbSet<Feedbacks> feedbacks { get; set; }
        public DbSet<News> news { get; set; }
        public DbSet<Post> posts { get; set; }
        public DbSet<Subject> subjects { get; set; }
    }
}
