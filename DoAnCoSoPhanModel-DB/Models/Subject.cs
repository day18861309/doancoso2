﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Subject
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên môn học.")]
        public string Name { get; set; }

        public string Description { get; set; }
        public List<Teacher>? teachers { get; set; }

    }
}
