﻿using DoAnCoSoPhanModel_DB.Models;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class StudentImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int studentId { get; set; }
        public Student? student { get; set; }
    }
}
