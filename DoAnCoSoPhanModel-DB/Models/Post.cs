﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Post
    {
        public Guid Id { get; set; }

        [Required]
        public string Content { get; set; }

        [ForeignKey("AuthorId")]
        public int UserId { get; set; }

        public DateTime PostedTime { get; set; }

        public int Likes { get; set; }

        public int Comments { get; set; }

        // Các thuộc tính khác liên quan
    }
}
