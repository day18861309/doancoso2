﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Teacher
    {
        public Guid Id { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập họ và tên.")]
        public string FullName { get; set; }

        public int Age { get; set; }

        public string Gender { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string EducationLevel { get; set; } // Bằng cấp: Đại học, Thạc sĩ, Tiến sĩ, Cử nhân

        [Required]
        public string WorkExperience { get; set; } // Kinh nghiệm làm việc

        public string TeachingInterest { get; set; } // Sở thích hoặc sở trường trong giảng dạy



    }
}
